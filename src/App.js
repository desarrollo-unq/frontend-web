import React from 'react';
import { Auth0Provider } from "@auth0/auth0-react";
import './App.css';
import { ThemeProvider, createMuiTheme } from '@material-ui/core';
import { red, green, orange } from '@material-ui/core/colors';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import CommerceRegister from './components/Register/CommerceRegister';
import Commerce from './components/HomeCommerce/Commerce'
import Register from './components/Register/Register';
import Login from './components/Login';
import Home from './components/Home'
import SuccessPage from './components/SuccessPage';
import MapExample from './components/Map';
import UserRegister from './components/Register/UserRegister';
import UserProfile from './components/UserProfile';
import ChangePassword from './components/ChangePassword';
import Buy from './components/Buy';
import BuyDetail from './components/BuyDetail';
import AdminCommerce from './components/AdminCommerce';
import { CartProvider } from './CartContext';

const theme = createMuiTheme({
  palette: {
    primary: red,
    secondary: orange,
    success: green,
    danger: green
  }
});
function App() {
  return (
    <Auth0Provider
      domain="dev-hde09xsz.us.auth0.com"
      clientId="vEQf2lCfjMiLIfNRxzgdFOcOy2XvqTsa"
      redirectUri="https://compramundo.herokuapp.com/"
      audience="https://dev-hde09xsz.us.auth0.com/api/v2/"
      scope="read:current_user update:current_user_metadata"
    >
      <CartProvider>
        <ThemeProvider theme={theme}>
          <BrowserRouter>
            <Switch>
              <Route path="/registration/commerce/success">
                <SuccessPage text="Se registró el comercio correctamente" />
              </Route>
              <Route path="/commerce/:id">
                {(props) => <Commerce {...props} />}
              </Route>
              <Route path="/registration/user/success">
                <SuccessPage text="Se registró el usuario correctamente" />
              </Route>
              <Route path="/registration/commerce" exact>
                <CommerceRegister />
              </Route>
              <Route path="/admin-commerce" exact>
                {props => <AdminCommerce {...props} />}
              </Route>
              <Route path="/user-profile" exact>
                <UserProfile />
              </Route>
              <Route path="/change-password" exact>
                <ChangePassword />
              </Route>
              <Route path="/registration/user" exact>
                <UserRegister />
              </Route>
              <Route path="/registration" exact>
                <Register />
              </Route>
              <Route path="/map-example" exact>
                <MapExample />
              </Route>
              <Route path="/login" exact>
                <Login />
              </Route>
              <Route path="/buy" exact>
                {props => <Buy {...props} />}
              </Route>
              <Route path="/buy-detail" exact>
                {BuyDetail}
              </Route>
              <Route path="/" exact>
                <Home />
              </Route>
            </Switch>
          </BrowserRouter>
        </ThemeProvider>
      </CartProvider>
    </Auth0Provider>
  )
}

export default App;
