import React, { createContext, useReducer } from 'react';

const initialState = {
  products: []
};
const store = createContext(initialState);
const { Provider } = store;

function reducer(state, action) {
  switch (action.type) {
    case 'add-product':
      state.products = [...state.products]
      state.products.push(action.item)
      return { ...state };
    default:
      throw new Error();
  };
}

const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, CartProvider }