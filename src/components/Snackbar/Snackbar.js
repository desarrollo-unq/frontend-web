import React, { useState } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const DURATION = 4000

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export function useSuccessSnackBar(message, vertical, horizontal){
  return useSnackBar(message, vertical, horizontal, "success", DURATION)
}

export function useWarningSnackBar(message, vertical, horizontal){
  return useSnackBar(message, vertical, horizontal, "warning", DURATION)
}

export function useDangerSnackBar(message, vertical, horizontal){
  return useSnackBar(message, vertical, horizontal, "danger", DURATION)
}

export function useSnackBar(message, vertical, horizontal, severity, duration) {
  const [open, setOpen] = useState(false)

  const openSnackbar = () => {
    setOpen(true);
  };

  const closeSnackbar = () => {
    setOpen(false);
  };

  return [
    () => <PositionedSnackbar
      message={message}
      vertical={vertical}
      horizontal={horizontal}
      closeSnackbar={closeSnackbar}
      open={open}
      severity={severity}
      duration={duration}
    />,
    openSnackbar
  ]
}

function PositionedSnackbar({ message, vertical, horizontal, open, closeSnackbar, severity, duration }) {
  return (
    <Snackbar
      anchorOrigin={{ vertical, horizontal }}
      open={open}
      onClose={closeSnackbar}
      key={vertical + horizontal}
      autoHideDuration={duration}
    >
      <Alert onClose={closeSnackbar} severity={severity}>
        {message}
      </Alert>
    </Snackbar>
  );
}
