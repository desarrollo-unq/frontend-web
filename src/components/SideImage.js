import React from 'react';
import { Grid, makeStyles } from "@material-ui/core";
import loginImage from '../resources/img/logo.png'

const useStyles = makeStyles((theme) => ({
  image: {
    backgroundImage: `url(${loginImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
}))

export default function SideImage(){
  const classes = useStyles();
  return <Grid item xs={false} sm={4} md={7} className={classes.image} />
}