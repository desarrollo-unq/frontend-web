import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useMenuAppBar } from './MenuAppBar';
import { TextField, Fab, Typography } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import { blue } from '@material-ui/core/colors';
import { updatePassword } from '../api/auth'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  fabBlue: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    backgroundColor: blue[500],
    '&:hover': {
      backgroundColor: blue[600],
    },
  },
  errors: {
    margin: theme.spacing(1, 0, 1, 2),
  },
  error:{
    display: 'flex',
    flexDirection: 'rows'
  }
}));


export default function ChangePassword() {
  const classes = useStyles()
  const [MenuAppBar] = useMenuAppBar()
  const [password, setPassword] = useState("")
  const [newPassword, setNewPassword] = useState("")
  const [repeatPassword, setRepeatPassword] = useState("")

  const [errors, setErrors] = useState([])
  function changePassword() {
    const ers = []
    if (newPassword.length < 7) {
      ers.push('Contraseña demasiado corta')
    }
    if (newPassword !== repeatPassword) {
      ers.push('Las contraseñas no coinciden')
    }
    if (newPassword === password) {
      ers.push('La contraseña no puede ser igual a la anterior')
    }
    setErrors(ers)
    if (ers.length===0){
      updatePassword(password, repeatPassword)
    }
  }

  return (
    <div className={classes.root}>
      <MenuAppBar />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="password"
        label="Contraseña Actual"
        type="password"
        id="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="new-password"
        label="Contraseña Nueva"
        type="password"
        id="new-password"
        value={newPassword}
        onChange={(e) => setNewPassword(e.target.value)}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        name="repeat-password"
        label="Repetir Contraseña"
        type="password"
        id="repeat-password"
        value={repeatPassword}
        onChange={(e) => setRepeatPassword(e.target.value)}
      />
      <div className={classes.errors} >
        {
          errors.length > 0 && <Typography variant="subtitle1">
            Hay errores:
        </Typography>
        }
        <div className={classes.error}>
          {errors.map(error => <Typography variant="caption">
            {error}
          </Typography>)}
        </div>
      </div>

      <Fab aria-label="edit" className={classes.fabBlue} onClick={changePassword}>
        <SaveIcon />
      </Fab>
    </div>
  );
}


