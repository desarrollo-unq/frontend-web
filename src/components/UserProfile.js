import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useTabs } from './Tabs'
import PersonPinIcon from '@material-ui/icons/PersonPin';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import { useMenuAppBar } from './MenuAppBar';
import { TextField, Fab, Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { saveUser } from '../api/auth'

import { orange, green } from '@material-ui/core/colors';
import { useHistory } from 'react-router-dom';
import PurchaseList from './PurchaseList';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  image: {
    maxHeight: '40px'
  },
  fabOrange: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    backgroundColor: orange[500],
    '&:hover': {
      backgroundColor: orange[600],
    },
  },
  fabGreen: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
}));


export default function UserProfile() {
  const history = useHistory()
  const classes = useStyles()
  const [MenuAppBar, user] = useMenuAppBar()
  const [email, setEmail] = useState(user ? user.email : "")
  const [name, setName] = useState(user ? user.first_name : "")
  const [last_name, setLastName] = useState(user ? user.last_name : "")
  const [editing, setEditing] = useState(false)
  const items = [
    {
      label: 'Usuario',
      icon: PersonPinIcon
    },
    {
      label: 'Compras',
      icon: ShoppingCartOutlinedIcon
    }
  ]
  const [Tabs, currentTab] = useTabs(items)

  function updateUser() {
    saveUser(email, name, last_name)
  }
  return (
    <div className={classes.root}>
      <MenuAppBar />
      <Tabs />
      {currentTab === 0 ? <>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          disabled={!editing}
          id="name"
          label="Nombre"
          name="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          autoFocus
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          disabled={!editing}
          id="last-name"
          label="Apellido"
          name="last-name"
          value={last_name}
          onChange={(e) => setLastName(e.target.value)}
          autoFocus
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          disabled={!editing}
          id="email"
          label="Correo Electrónico"
          name="email"
          autoComplete="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          autoFocus
        />
        <Button
          variant="contained"
          color="secondary"
          className={classes.button}
          onClick={()=>history.push('/change-password')}
          startIcon={<LockOutlinedIcon />}
        >
          Cambiar contraseña
      </Button>
        <Fab aria-label="edit" className={editing ? classes.fabGreen : classes.fabOrange} onClick={editing ? () => {
          setEditing(!editing)
          updateUser()
        } : () => setEditing(!editing)}>
          {editing ? <SaveIcon /> : <EditIcon />}
        </Fab>
      </> : <PurchaseList user={user}/>}
    </div>
  );
}


