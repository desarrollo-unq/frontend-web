import React, { useEffect, useState } from 'react';
import { getByUser } from '../api/purchase';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Purchase from './Purchase';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  totalCard: {
    backgroundColor: theme.palette.secondary.paper,
    margin: theme.spacing(1, 1),
    padding: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  totalCardContent: {
    padding: theme.spacing(0)
  },
  cardContent: {
    flexGrow: 1,
  },
  textRight: {
    textAlign: 'right',
    textDecoration: theme.typography.fontWeightBold
  },
  button: {
    margin: theme.spacing(1),
  },
}));


export default function PurchaseList({ user }) {
  const classes = useStyles()
  const [purchases, setPurchases] = useState([])

  useEffect(() => {
    getByUser(user).then(results => {
      setPurchases(results)
    })
  }, [user])
  return (

    <Grid container className={classes.root}>
      {
        purchases.map(purchase => <Purchase purchase={purchase} />)
      }
    </Grid>
  )
}


