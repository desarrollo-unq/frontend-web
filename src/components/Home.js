import React, {useState, useEffect} from 'react'
import Products from './HomeContent/Products'
import Main from './Main'
import productProvider from '../api/product';


export default function Home() {

  const [products, setProducts] = useState([])

  useEffect(() => {
    productProvider.getAll().then(products => {
      setProducts(products)
    })
  
}, [setProducts])


  return <Main showWelcome={true} Content={(props) => <Products {...props} products={products} showCommerceButton={true} showAdminButton={false}/>} />
}