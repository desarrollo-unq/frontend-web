import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { TextField } from '@material-ui/core';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import { Grid } from '@material-ui/core'
import productProvider from '../api/product'
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  margin: {
    marginTop: theme.spacing(1),
  },
  mainContainer: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  left: {
    marginLeft: theme.spacing(1),
  },
  right: {
    marginRight: theme.spacing(1),
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

export function useModalEditionProduct(p){
    const [open, setOpen] = useState(false)
    const [typeModal, setTypeModal] = useState(false)
    const product = useState(p)

    const openModal = () => {
        setOpen(true);
      };
    
      const closeModal = () => {
        setOpen(false);
      };

      const setTypeModalReducer = (value) => {
        /*
          DEFINITION TYPE MODAL:
            value ? typeModal = DeleteModal : typeModal = EditionModal 
        */
        setTypeModal(value)
      }

    return [
        () => <AlertDialogSlide 
            open={open}
            closeModal={closeModal}
            typeModal={typeModal}
            product={product}

        />,
        openModal,
        setTypeModalReducer,
    ]
}

function AlertDialogSlide({open, closeModal, typeModal, product}) {
  const classes = useStyles();
  const productToHandle = product[0]
  const { t } = useTranslation()
  const [name, setName] = useState(productToHandle.name)
  const [trademark, setTrademark] = useState(productToHandle.trademark)
  const [price, setPrice] = useState(productToHandle.price)
  const [stock, setStock] = useState(productToHandle.stock)
  const [image, setImage] = useState(productToHandle.image)
  const commerce_id = productToHandle.commerce.id
  const category_id = productToHandle.category.id

  function deleteProduct(){
    productProvider.deleteProduct(productToHandle.id)
                    .then( () => { closeModal(); window.location.reload()})
                    .catch(error => { console.log(error) })
  }

  function editProduct(){
    productProvider.updateProduct(productToHandle.id, name, trademark, price, stock, image, commerce_id, category_id)
                   .then(() => { closeModal(); window.location.reload()})
                   .catch(error => { console.log(error) })
  }

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={closeModal}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        {/********** Delete product **********/}
        {typeModal && <>
            <DialogTitle id="alert-dialog-slide-title">{"Eliminar producto"}</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                  {t('MoldaEditionProduct.textDelete')}
                </DialogContentText>
            </DialogContent>
            </>
        }

        {/********** Edit product **********/}
        {!typeModal && <>
            <DialogTitle id="alert-dialog-slide-title">{"Editar producto"}</DialogTitle>
            <DialogContent>
              {/********** Name **********/}
            <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                disabled={false}
                id="name"
                label="Nombre"
                name="name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                autoFocus
                InputLabelProps={{shrink:true}}
                />
                {/********** Trademark **********/}
              <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  disabled={false}
                  id="name"
                  label="Marca"
                  name="name"
                  value={trademark}
                  onChange={(e) => setTrademark(e.target.value)}
                  autoFocus
                  InputLabelProps={{shrink:true}}
              />
              {/********** Price & Stock**********/}
              <Grid md="12" className={classes.mainContainer}>
                <Grid md="6" className={classes.right}>
                  <FormControl fullWidth variant="outlined" className={classes.margin}>
                    <InputLabel htmlFor="outlined-adornment-amount">Precio</InputLabel>
                    <OutlinedInput
                      id="outlined-adornment-amount"
                      value={price}
                      onChange={(e) => setPrice(e.target.value)}
                      startAdornment={<InputAdornment required position="start">$</InputAdornment>}
                      labelWidth={60}
                    />

                  </FormControl>
                </Grid>
                <Grid md="6" className={classes.left}> 
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    disabled={false}
                    id="name"
                    label="Stock"
                    name="name"
                    value={stock}
                    onChange={(e) => setStock(e.target.value)}
                    autoFocus
                    InputLabelProps={{shrink:true}}
                  />
                </Grid>

              </Grid>
              {/********** Image **********/}
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                disabled={false}
                id="name"
                label="Imagen"
                name="name"
                value={image}
                onChange={(e) => setImage(e.target.value)}
                autoFocus
                InputLabelProps={{shrink:true}}
                />

            </DialogContent>
            </>
        }

        <DialogActions>
          <Button onClick={closeModal} color="primary">
            {t('MoldaEditionProduct.cancel')}
          </Button>
          <Button onClick={()=>{typeModal ? deleteProduct() : editProduct()}} color="primary">
            {t('MoldaEditionProduct.accept')}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
