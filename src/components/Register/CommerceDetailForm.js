import React from 'react'
import { Select, MenuItem, FormControl, InputLabel, FormHelperText, makeStyles, Checkbox, FormControlLabel } from '@material-ui/core'
import { useTranslation } from 'react-i18next';


const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(2, 0, 0),
    minWidth: '100%',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


export function useCommerceDetailForm(){
  const {t} = useTranslation()
  const categoryError = t('CommerceDetailForm.Errors.category')
  const paymentError = t('CommerceDetailForm.Errors.payment')
  return [CommerceDetailForm, (category, selectedPaymentMethods) => validateCommerceDetailForm(category, selectedPaymentMethods, categoryError, paymentError)]
}


function validateCommerceDetailForm(category, selectedPaymentMethods, categoryError, paymentError) {
  const errors = {
    category: [],
    payment: [],
  }
  if (category===0) {
    errors.category = [categoryError]
  }

  if (selectedPaymentMethods.length === 0) {
    errors.payment = [paymentError]
  }
  return [!(Object.keys(errors).some(k => errors[k].length !== 0)), errors]
}

export default function CommerceDetailForm({
  commerceCategories,
  paymentMethods,
  selectedCategory,
  setSelectedCategory,
  selectedPaymentMethods,
  setSelectedPaymentMethods,
  errors
}) {
  const classes = useStyles()
  const {t} = useTranslation()
  return (
    <>
      <FormControl className={classes.formControl} error={errors.category && errors.category.length > 0}>
        <InputLabel shrink id="category-label">
          {t('CommerceDetailForm.Fields.category')}
        </InputLabel>
        <Select
          id="category"
          value={selectedCategory || 0}
          onChange={e => setSelectedCategory(e.target.value)}
        >
          <MenuItem value={0} disabled>
            {t('CommerceDetailForm.Fields.select_category')}
          </MenuItem>
          {commerceCategories.map(cc => {
            return <MenuItem value={cc.id} key={cc.id}>{cc.name}</MenuItem>
          })}
        </Select>
        <FormHelperText>{errors.category && errors.category.join(". ")}</FormHelperText>
      </FormControl>
      <FormControl className={classes.formControl} error={errors.payment && errors.payment.length > 0}>
        <InputLabel shrink id="payment-label">
          {t('CommerceDetailForm.Fields.payment')}
        </InputLabel>
          {paymentMethods.map(cc => {
            return <FormControlLabel 
              control={<Checkbox
                id="payment"
                color="primary"
                key={`pm-${cc.id}`}
                checked={selectedPaymentMethods.some(pm => pm === cc.id)}
                style={{margin:'10px'}}
                onChange={e => {
                  if (e.target.checked)
                    setSelectedPaymentMethods([...selectedPaymentMethods, cc.id])
                  else
                    setSelectedPaymentMethods(selectedPaymentMethods.filter(pm => cc.id!==pm))
                }}
              />}
              label={cc.name}
            />
          })}
        <FormHelperText>{errors.payment && errors.payment.join(". ")}</FormHelperText>
      </FormControl>

    </>
  )
}