import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { register } from '../../api/auth';
import { CircularProgress } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useUserForm } from './UserForm';
import SideImage from '../SideImage';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(2, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.common.white,
    width: "30%",
    height: "30%",
  },
  submit: {
    margin: theme.spacing(3, 2, 2),
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    margin: theme.spacing(2)
  },
  prevNextButton: {
    margin: theme.spacing(0, 2)
  },
  chip: {
    margin: theme.spacing(2, 4)
  },
  register: {
    margin: theme.spacing(0, 0, 2)
  }
}));

export default function UserRegister() {
  const classes = useStyles();
  const history = useHistory();
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [repeatPassword, setRepeatPassword] = useState("")
  const [userErrors, setUserErrors] = useState({})
  const { t } = useTranslation()

  const [UserForm, validateUserForm] = useUserForm()
  const props = {
    email,
    setEmail,
    password,
    setPassword,
    repeatPassword,
    setRepeatPassword,
    errors: userErrors
  }
  function handleLogin() {
    setLoading(true)
    register(email, password)
      .then(() => {
        history.push('/registration/user/success')
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <SideImage />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            {t('UserRegister.register_user')}
          </Typography>
          <UserForm {...props} />
          <div className={classes.buttons}>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.register}
              onClick={() => {
                const validation = validateUserForm(email, password, repeatPassword)
                setUserErrors(validation[1])
                if (validation[0])
                  handleLogin()
              }}>
              {t('UserRegister.do_register')}
            </Button>
          </div>
          {loading && <>
            <Typography variant="subtitle1">
              {t('UserRegister.loader_message')}
            </Typography>
            <CircularProgress />
          </>
          }
        </div>
      </Grid>
    </Grid>
  );
}