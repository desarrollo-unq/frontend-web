import React from 'react'
import { TextField } from '@material-ui/core'
import { useTranslation } from 'react-i18next'


export function useCommerceForm(){
  const {t} = useTranslation()
  const nameError = t('CommerceForm.Errors.name_too_short')
  const addressError = t('CommerceForm.Errors.invalid_address')
  const distanceError = t('CommerceForm.Errors.invalid_distance')
  return [CommerceForm, (name, address, distance) => validateCommerceForm(name, address, distance, nameError, addressError, distanceError)]
}

function validateCommerceForm(name, address, distance, nameError, addressError, distanceError) {
  const errors = {
    name: [],
    address: [],
    distance: []
  }
  if (name.length <= 2) {
    errors.name = [...errors.name, nameError]
  }

  if (address.length <= 3) {
    errors.address = [...errors.address, addressError]
  }

  if (distance <= 0) {
    errors.distance = [...errors.distance, distanceError]
  }

  return [!(Object.keys(errors).some(k => errors[k].length !== 0)), errors]
}

export default function CommerceForm({
  commerceName,
  setCommerceName,
  commerceAddress,
  setCommerceAddress,
  commerceMaxDistance,
  setCommerceMaxDistance,
  errors
}) {
  const {t} = useTranslation()
  return (
    <>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="name"
        label={t('CommerceForm.Fields.name')}
        name="name"
        value={commerceName}
        onChange={e=>setCommerceName(e.target.value)}
        error={errors.name && errors.name.length>0}
        helperText={errors.name && errors.name.join(". ")}
        autoFocus
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="address"
        label={t('CommerceForm.Fields.address')}
        name="address"
        value={commerceAddress}
        onChange={e=>setCommerceAddress(e.target.value)}        
        error={errors.address && errors.address.length>0}
        helperText={errors.address && errors.address.join(". ")}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="distance"
        label={t('CommerceForm.Fields.distance')}
        name="distance"
        value={commerceMaxDistance}
        onChange={e=>setCommerceMaxDistance(e.target.value)}
        error={errors.distance && errors.distance.length>0}
        helperText={errors.distance && errors.distance.join(". ")}
      />
    </>
  )
}