import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import { Button } from '@material-ui/core';
import ShoppingCartOutlinedIcon from '@material-ui/icons/ShoppingCartOutlined';
import StoreOutlinedIcon from '@material-ui/icons/StoreOutlined';
import SideImage from '../SideImage';
import { useTranslation } from 'react-i18next';


const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(2, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.common.white,
    width: "30%",
    height: "30%",
  },
  submit: {
    margin: theme.spacing(3, 2, 2),
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    margin: theme.spacing(2, 2, 0)
  },
  title: {
    margin: theme.spacing(3, 2, 2),
  }
}));


export default function Register() {
  const classes = useStyles();
  const history = useHistory();
  const {t} = useTranslation()

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <SideImage />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Typography component="h1" variant="h5" className={classes.title} >
            {t('Register.question')}
          </Typography>
          <div style={{display:'flex', flexDirection:'column'}}>
          <Button
            variant="contained"
            color="primary"
            fullWidth
            className={classes.buttons}
            startIcon={<ShoppingCartOutlinedIcon />}
            onClick={() => history.push('/registration/user')}>
            {t('Register.buyer')}
          </Button>
          <Button
            variant="contained"
            color="primary"
            fullWidth
            className={classes.buttons}
            startIcon={<StoreOutlinedIcon />}
            onClick={() => history.push('/registration/commerce')}>
            {t('Register.commerce')}
          </Button>
          </div>
        </div>
      </Grid>
    </Grid>
  );
}