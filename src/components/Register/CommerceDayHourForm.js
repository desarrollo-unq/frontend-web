import React from 'react'
import { FormControl, InputLabel, makeStyles, Button, Divider } from '@material-ui/core'
import CommerceDayHour from './CommerceDayHour';
import * as moment from 'moment'
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(2, 0, 0),
    minWidth: '100%',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


export function useCommerceDayHourForm() {
  return [CommerceDayHourForm, validateCommerceDayHourForm]
}


function validateCommerceDayHourForm(dayHours) {
  const errors = dayHours.map(dayhour => dayhour.closingHour.isBefore(dayhour.openingHour))
  
  return [!(errors.some(t => t)), errors]
}

export default function CommerceDayHourForm({
  dayHours,
  setDayHours,
  errors
}) {
  const classes = useStyles()
  const {t} = useTranslation()

  function handleDayChange(index) {
    return (value) => {
      const newDayHours = [...dayHours]
      newDayHours[index] = {
        ...newDayHours[index],
        day: value
      }
      setDayHours(newDayHours)
    }
  }

  function handleOpeningChange(index) {
    return (value) => {
      const newDayHours = [...dayHours]
      newDayHours[index] = {
        ...newDayHours[index],
        openingHour: value
      }
      setDayHours(newDayHours)
    }
  }

  function handleClosingChange(index) {
    return (value) => {
      const newDayHours = [...dayHours]
      newDayHours[index] = {
        ...newDayHours[index],
        closingHour: value
      }
      setDayHours(newDayHours)
    }
  }

  function addDayHour() {
    const newDayHours = [...dayHours]
    newDayHours.push({ day: 1, openingHour: moment(), closingHour: moment() })
    setDayHours(newDayHours)
  }

  return (
    <>
      <FormControl className={classes.formControl} error={errors.some(t => t)}>
        <InputLabel shrink id="category-label">
          {t('CommerceDayHourForm.Fields.schedule')}
        </InputLabel>
        {
          dayHours.map((dayHour, index) =>
            <>
              <CommerceDayHour
                selectedDay={dayHour.day}
                handleDayChange={handleDayChange(index)}
                openingHour={dayHour.openingHour}
                handleOpeningChange={handleOpeningChange(index)}
                closingHour={dayHour.closingHour}
                handleClosingChange={handleClosingChange(index)}
                errors={errors[index]} />
              <Divider />
            </>)
        }

      </FormControl>

      <Button
        variant="contained"
        color="primary"
        className={classes.prevNextButton}
        onClick={addDayHour}
      >
        {t('CommerceDayHourForm.Fields.add_schedule')}
      </Button>
    </>
  )
}