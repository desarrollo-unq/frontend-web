import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { registerCommerce, addPaymentMethods } from '../../api/commerce';
import { CircularProgress } from '@material-ui/core';
import { useCommerceForm } from './CommerceForm';
import { useStepper } from './Stepper';
import commerceCategoryProvider from '../../api/commerce-category'
import paymentMethodProvider from '../../api/payment-method'
import { useCommerceDetailForm } from './CommerceDetailForm';
import { useHistory } from 'react-router-dom';
import { useUserForm } from './UserForm';
import { register, getUser } from '../../api/auth';
import SideImage from '../SideImage';
import { useCommerceDayHourForm } from './CommerceDayHourForm';
import * as moment from 'moment'
import { addDayHours } from '../../api/day-hour';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(2, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.common.white,
    width: "30%",
    height: "30%",
  },
  submit: {
    margin: theme.spacing(3, 2, 2),
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    margin: theme.spacing(2)
  },
  prevNextButton: {
    margin: theme.spacing(0, 2)
  },
  chip: {
    margin: theme.spacing(2, 4)
  },
  register: {
    margin: theme.spacing(0, 0, 2)
  }
}));


export default function CommerceRegister() {
  const classes = useStyles();
  const history = useHistory();
  const {t} = useTranslation()
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [repeatPassword, setRepeatPassword] = useState("")
  const [userErrors, setUserErrors] = useState({})
  const [commerceName, setCommerceName] = useState("")
  const [commerceAddress, setCommerceAddress] = useState("")
  const [commerceMaxDistance, setCommerceMaxDistance] = useState(0)
  const [commerceErrors, setCommerceErrors] = useState({})
  const [commerceCategories, setCommerceCategories] = useState([])
  const [paymentMethods, setPaymentMethods] = useState([])
  const [selectedCategory, setSelectedCategory] =  useState(0)
  const [selectedPaymentMethods, setSelectedPaymentMethods] =  useState([])
  const [dayHours, setDayHours] =  useState([{day:1, openingHour:moment(), closingHour:moment()}])
  const [commerceDetailErrors, setCommerceDetailErrors] = useState({})
  const [commerceDayHourErrors, setCommerceDayHourErrors] = useState([])

  useEffect(()=>{
    commerceCategoryProvider.getAll().then(categories=>setCommerceCategories(categories))
    paymentMethodProvider.getAll().then(methods=>setPaymentMethods(methods))
  }, [setCommerceCategories, setPaymentMethods])
  
  const [UserForm, validateUserForm] = useUserForm()
  const [CommerceForm, validateCommerceForm] = useCommerceForm()
  const [CommerceDetailForm, validateCommerceDetailForm] = useCommerceDetailForm()
  const [CommerceDayHourForm, validateCommerceDayHourForm] =  useCommerceDayHourForm()
  const childs = [
    {
      Form: UserForm,
      props: {
        email,
        setEmail,
        password,
        setPassword,
        repeatPassword,
        setRepeatPassword,
        errors: userErrors
      },
      validate: () => validateUserForm(email, password, repeatPassword),
      setErrors: setUserErrors,
      handleNext: registerUser,
      label: t('CommerceRegister.Stepper.Titles.user_data')
    }, {
      Form: CommerceForm,
      props: {
        commerceName,
        setCommerceName,
        commerceAddress,
        setCommerceAddress,
        commerceMaxDistance,
        setCommerceMaxDistance,
        errors: commerceErrors
      },
      validate: () => validateCommerceForm(commerceName, commerceAddress, commerceMaxDistance),
      setErrors: setCommerceErrors,
      label: t('CommerceRegister.Stepper.Titles.commerce_data')
    },
    {
      Form: CommerceDetailForm,
      props: {
        commerceCategories,
        paymentMethods,
        selectedCategory,
        setSelectedCategory,
        selectedPaymentMethods,
        setSelectedPaymentMethods,
        errors: commerceDetailErrors
      },
      validate: () => validateCommerceDetailForm(selectedCategory, selectedPaymentMethods),
      setErrors: setCommerceDetailErrors,
      label: t('CommerceRegister.Stepper.Titles.commerce_detail')
    },
    {
      Form: CommerceDayHourForm,
      props: {
        dayHours,
        setDayHours,
        errors: commerceDayHourErrors
      },
      validate: () => validateCommerceDayHourForm(dayHours),
      setErrors: setCommerceDayHourErrors,
      label: t('CommerceRegister.Stepper.Titles.commerce_hours')

    }
  ]
  const [Stepper, handleNext, handleBack, activeStep] = useStepper(childs.map(c => c.label))
  function handleRegister() {
    setLoading(true)
    registerCommerce(getUser().id, commerceName, commerceAddress, commerceMaxDistance, selectedCategory)
      .then((commerce)=>{
        addPaymentMethods(commerce, selectedPaymentMethods)
        .then(() =>{
          addDayHours(commerce, dayHours)
          .then(() => {
            history.push('/registration/commerce/success')
          })
        })
      })
      .finally(() => {
        setLoading(false)
      })
  }

  function registerUser(){
    setLoading(true)
    register(email, password).then(()=>{
      handleNext()
    })
    .finally(() => {
      setLoading(false)
    })
  }

  const CurrentChild = childs[activeStep].Form
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <SideImage />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            {t('CommerceRegister.title')}
          </Typography>
          <Stepper />
          <CurrentChild {...childs[activeStep].props} />
          <div className={classes.buttons}>
            <Button
              disabled={activeStep === 0}
              onClick={handleBack}
              className={classes.prevNextButton}
            >
              {t('CommerceRegister.Stepper.Buttons.previous')}
            </Button>
            <Button 
              disabled={activeStep === childs.length - 1} 
              variant="contained" 
              color="primary" 
              className={classes.prevNextButton}
              onClick={() => {
                const validation = childs[activeStep].validate()
                childs[activeStep].setErrors(validation[1])
                if (validation[0])
                  (childs[activeStep].handleNext || handleNext)()
            }}>
              {t('CommerceRegister.Stepper.Buttons.next')}
            </Button>

          </div>
          {activeStep === childs.length - 1 && 
            <Button 
              fullWidth 
              variant="contained" 
              color="primary" 
              className={classes.register}
              onClick={() => {
                const validation = childs[activeStep].validate()
                childs[activeStep].setErrors(validation[1])
                if (validation[0])
                  handleRegister()
              }}>
              {t('CommerceRegister.register_button')}
            </Button>}
          {loading && <CircularProgress />}
        </div>
      </Grid>
    </Grid>
  );
}