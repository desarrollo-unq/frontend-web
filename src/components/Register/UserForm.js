import React from 'react'
import { TextField } from '@material-ui/core'
import { useTranslation } from 'react-i18next';


export function useUserForm() {
  const {t} = useTranslation()
  const invalidEmail = t('UserForm.Errors.invalid_email')
  const shortPassword = t('UserForm.Errors.short_password')
  const passwordsUnmatch = t('UserForm.Errors.passwords_unmatch')
  return [UserForm, (email, password, repeatPassword) => validateUserForm(email, password, repeatPassword, invalidEmail, shortPassword, passwordsUnmatch)]
}

function validateUserForm(email, password, repeatPassword, invalidEmail, shortPassword, passwordsUnmatch) {
  var re = /\S+@\S+\.\S+/;
  const errors = {
    email: [],
    password: [],
    repeatPassword: []
  }
  if (!re.test(email)) {
    errors.email = [invalidEmail]
  }

  if (password.length <= 7) {
    errors.password = [...errors.password, shortPassword]
  }

  if (password !== repeatPassword) {
    errors.password = [...errors.password, passwordsUnmatch]
    errors.repeatPassword = [...errors.repeatPassword, passwordsUnmatch]
  }

  return [!(Object.keys(errors).some(k => errors[k].length !== 0)), errors]
}

function UserTextField(props) {
  return <TextField
    variant="outlined"
    margin="normal"
    required
    fullWidth
    {...props} />
}

export default function UserForm({ email, setEmail, password, setPassword, repeatPassword, setRepeatPassword, errors }) {
  const {t} = useTranslation()
  return (
    <>
      <UserTextField
        key="email"
        label={t('UserForm.Fields.email')}
        name="email"
        autoComplete="email"
        error={errors.email && errors.email.length > 0}
        helperText={errors.email && errors.email.join(". ")}
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      />
      <UserTextField
        name="password"
        error={errors.password && errors.password.length > 0}
        helperText={errors.password && errors.password.join(". ")}
        label={t('UserForm.Fields.password')}
        type="password"
        key="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <UserTextField
        name="repeat-password"
        error={errors.repeatPassword && errors.repeatPassword.length > 0}
        helperText={errors.repeatPassword && errors.repeatPassword.join(". ")}
        label={t('UserForm.Fields.repeat_password')}
        type="password"
        key="repeat-password"
        value={repeatPassword}
        onChange={(e) => setRepeatPassword(e.target.value)}
      />
    </>
  )
}