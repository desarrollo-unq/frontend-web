import React from 'react'
import { FormControl, FormHelperText, makeStyles, Select, MenuItem } from '@material-ui/core'
import { MuiPickersUtilsProvider, KeyboardTimePicker } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import * as moment from 'moment'
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(2, 0, 0),
    minWidth: '100%',
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));


export function useCommerceDayHour() {
  return [CommerceDayHour, validateCommerceDayHour]
}


function validateCommerceDayHour(category, selectedPaymentMethods) {
  const errors = []

  return [!(Object.keys(errors).some(k => errors[k].length !== 0)), errors]
}

export default function CommerceDayHour({
  openingHour,
  handleOpeningChange,
  closingHour,
  handleClosingChange,
  selectedDay,
  handleDayChange,
  errors
}) {
  const classes = useStyles()
  const {t} = useTranslation()

  return (
    <MuiPickersUtilsProvider utils={MomentUtils}>
      <FormControl className={classes.formControl} error={errors}>
        <Select
          id="category"
          value={selectedDay}
          onChange={e => handleDayChange(e.target.value)}
        >
          {moment.weekdays().map((cc, index) => <MenuItem value={index} key={index}>{t(`Days.${cc}`)}</MenuItem>)}
        </Select>
        <KeyboardTimePicker
          margin="normal"
          id="time-picker"
          label={t('CommerceDayHour.Fields.opening_hour')}
          value={openingHour}
          onChange={handleOpeningChange}
          KeyboardButtonProps={{
            'aria-label': 'change time',
          }}
        />
        <KeyboardTimePicker
          margin="normal"
          id="time-picker"
          label={t('CommerceDayHour.Fields.closing_hour')}
          value={closingHour}
          onChange={handleClosingChange}
          KeyboardButtonProps={{
            'aria-label': 'change time',
          }}
        />
        <FormHelperText>{errors && t('CommerceDayHour.Errors.invalid_hours')}</FormHelperText>
      </FormControl>
    </MuiPickersUtilsProvider>
  )
}

