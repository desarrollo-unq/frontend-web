import React, { useState } from 'react';
import { TextField } from '@material-ui/core';

export function useFormEditCreateProduct(){
    const [product, setProduct] = useState([])
    const [typeForm, setTypeForm] = useState(false)

    const setTypeFormReducer = (typrForm) => {
        setTypeForm(typrForm)
      }

    const setProductReducer = (product) => {
        setProduct(product)
    }

    return [
        () => <FormEditCreateProduct
            typeModal={typeForm}
            product={product}
        />,
        setProductReducer,
        setTypeFormReducer
    ]
}

/*
    typeForm ? create produt : edit product
*/

function FormEditCreateProduct({typeForm}) {
  return (
    <div>
        {!typeForm && <>
            <TextField
                id="standard-full-width"
                label="Nombre"
                fullWidth
                margin="normal"
                InputLabelProps={{shrink:true}}
            />
            <TextField
                id="standard-full-width"
                label="Marca"
                fullWidth
                margin="normal"
                InputLabelProps={{shrink:true}}
            />
        </>
        }
    </div>
  );
}
