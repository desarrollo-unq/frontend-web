import React, { useState } from 'react';
import { Grid, Typography, Button, Paper, useTheme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  totalCard: {
    backgroundColor: theme.palette.secondary.paper,
    margin: theme.spacing(1, 1),
    padding: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  totalCardContent: {
    padding: theme.spacing(0)
  },
  cardContent: {
    flexGrow: 1,
  },
  textRight: {
    textAlign: 'right',
    textDecoration: theme.typography.fontWeightBold
  },
  button: {
    margin: theme.spacing(1),
  },
}));


export default function Purchase({ purchase }) {
  const classes = useStyles()
  const theme = useTheme()
  const { t } = useTranslation()
  const [showDetail, setShowDetail] = useState(false)

  return (
    <Grid item xs={12}>
      <Paper elevation={3} className={classes.totalCard}>
        <Typography gutterBottom variant="h5">
          {`${t("PurchaseList.purchaseNbr")}: ${purchase.id}`}
        </Typography>
        <Typography gutterBottom variant="body1">
          {`${t("PurchaseList.total")} $${purchase.purchase_commerces.reduce((ac, cur) => {
            return ac + parseFloat(cur.total)
          }, 0)}`}
        </Typography>
        <Button variant="outlined" size="small" color="primary" className={classes.margin} onClick={() => setShowDetail(!showDetail)}>
          {showDetail ? t("PurchaseList.hideDetails") : t("PurchaseList.seeDetails")}
        </Button>
        {
          showDetail && (
            <Grid container>
              {purchase.purchase_commerces.map(pc => {
                return (
                  <Paper elevation={1} style={{ margin: theme.spacing(2), padding: theme.spacing(2) }}>
                    <Grid item xs={12} sm={6}>
                      <Typography gutterBottom variant="h6">
                        {pc.commerce.name} ${pc.total}
                      </Typography>
                      <Grid container>
                        {
                          pc.products.map(prod => {
                            return (
                              <Grid item xs={12}>
                                <Typography variant="subtitle1">
                                  {prod.product.name} x{prod.amount}
                                </Typography>
                              </Grid>
                            )
                          })
                        }
                      </Grid>
                    </Grid>
                  </Paper>
                )
              })}
            </Grid>
          )
        }
      </Paper>
    </Grid>
  )
}


