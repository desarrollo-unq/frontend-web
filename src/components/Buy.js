import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Card, CardContent, Typography, Button } from '@material-ui/core';
import Main from './Main';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { store } from '../CartContext';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  totalCard:{
    backgroundColor: theme.palette.secondary.paper,
    margin: theme.spacing(1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  totalCardContent:{
    padding: theme.spacing(0)
  },
  cardContent: {
    flexGrow: 1,
  },
  textRight: {
    textAlign: 'right',
    textDecoration: theme.typography.fontWeightBold
  },
  button: {
    margin: theme.spacing(1),
  },
}));

function calculateTotal(item){
  return item.product.price * item.amount
}

function Content(props) {
  const history = useHistory()
  const classes = useStyles();
  const items = useContext(store).state.products
  const {t} = useTranslation()

  return <Grid container className={classes.root}>
    <Grid item xs={12}>
      <Card className={classes.totalCard}>
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
          Total ${items.reduce((prev, item)=>{
              return prev + calculateTotal(item)
            }, 0)}
          </Typography>
          <Button
            variant="contained"
            color="primary"
            size="small"
            fullWidth
            onClick={() =>{history.push('/buy-detail', items)}}
          >
            {t('Buy.buy')}
          </Button>
        </CardContent>
      </Card>
    </Grid>
    {items.map(item => {
      return <Grid item xs={12}>
        <Card className={classes.card}>
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom variant="h5" component="h2">
              {item.product.name}
            </Typography>
            <Typography gutterBottom variant="h6" component="h6">
              {item.product.trademark}
            </Typography>
            <Typography>
              ${item.product.price} x {item.amount}
            </Typography>
            <Typography className={classes.textRight}>
              ${item.product.price * item.amount}
            </Typography>
          </CardContent>
        </Card>
      </Grid>
    })}
  </Grid>
}
export default function Buy(props) {
  return <Main showWelcome={false} Content={() => <Content {...props} />} />
}
