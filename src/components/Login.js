import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import logo from '../resources/img/logo.png'
import { login } from '../api/auth';
import { CircularProgress, useTheme } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import SideImage from './SideImage';
import { useAuth0 } from '@auth0/auth0-react';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  paper: {
    margin: theme.spacing(4, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.common.white,
    width: "30%",
    height: "30%",
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Login() {
  const classes = useStyles();
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const history = useHistory()
  const theme = useTheme()
  const { loginWithRedirect } = useAuth0()
  const { t } = useTranslation()
  const [loading, setLoading] = useState(false)

  function handleLogin() {
    setLoading(true)
    login(email, password).then(() => {
      history.push("/", {hola:true})
    })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <SideImage />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <img src={logo} alt="logo" />
          </Avatar>
          <Typography component="h1" variant="h5">
            Bienvenidx a CompraMundo
          </Typography>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Correo Electrónico"
            name="email"
            autoComplete="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Contraseña"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button
            type="button"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleLogin}
          >
            {t("Login.login")}
          </Button>
          <Button
            type="button"
            fullWidth
            variant="outlined"
            color="primary"
            onClick={loginWithRedirect}
          >
            <img alt="google icon" width={theme.spacing(2)}src="https://img.icons8.com/cute-clipart/64/000000/google-logo.png" style={{marginRight: theme.spacing(1)}}/>
            {t("Login.login-social")}
          </Button>
          {loading && <CircularProgress />}
          <LoginExtraOptions />
        </div>
      </Grid>
    </Grid>
  );
}

function LoginExtraOptions() {
  const history = useHistory()
  const theme = useTheme()
  return (
    <Grid container  style={{marginTop: theme.spacing(1)}} >
      <Grid item>
        <Link href="#" variant="body2" onClick={() => history.push('/registration')}>
          {"No tenes cuenta? Creala"}
        </Link>
      </Grid>
    </Grid>
  )
}