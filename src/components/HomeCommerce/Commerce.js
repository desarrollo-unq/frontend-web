import React, { useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import Products from '../HomeContent/Products';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import { useHistory } from 'react-router-dom';
import dayHourProvier from '../../api/day-hour';
import { getProductByCommerce } from '../../api/product';
import Main from '../Main';
import * as moment from 'moment'
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({

  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  textOrage: {
    color: "#f44336",
  },
  column: {
    display: 'flex',
    flexDirection: 'column',
  }

}));


export default function Commerce(props) {
  const classes = useStyles();
  const history = useHistory();
  const commerce = props.location.state
  const [dayHours, setDaysHours] = useState([])
  const [products, setProducts] = useState([])
  const {t} = useTranslation()

  function getNameDay(day) {
    return t(`Days.${moment.weekdays(day)}`)
  }

  //pasar mas info para pintar si esta en la hora y el dia actual o sea si el comercio esta abierto ahora
  function formatHour(hour) {
    return moment(hour, "HH:mm:ss").format('HH:mm')
  }

  function isAvailable() {
    return dayHours.some(available)
  }

  function available(dayHour){
    const now = moment()
    return sameWeekDay(dayHour, now) && inHourRange(dayHour, now)
  }

  function sameWeekDay(dayHour, now){
    return now.weekday() === dayHour.day
  }

  function inHourRange(dayHour, now){
    return now.isSameOrAfter(moment(dayHour.opening_hour, "HH:mm:ss")) && now.isSameOrBefore(moment(dayHour.closing_hour, "HH:mm:ss"))
  }


  useEffect(() => {
    dayHourProvier.getAllByCommerce(commerce.id).then(dh => setDaysHours(dh))
    getProductByCommerce(commerce.id).then(setProducts)
  }, [commerce, setDaysHours, setProducts])

  return (
    <Main showWelcome={true} Content={(props) => (
      <Container className={classes.row} maxwidth="md">
        <Grid item xs={3}>
          <Card className={classes.root} variant="outlined">
            <CardContent>

              <Typography variant="h5" component="h2">
                {commerce.name}
              </Typography>
              <hr></hr>
              <Typography className={classes.pos} color="textSecondary">
                {isAvailable() ? t('Commerce.open') : t('Commerce.closed')}
              </Typography>
              <Grid>
                {dayHours.map((d) => (
                  <Typography variant="body2">
                    {getNameDay(d.day)}: {formatHour(d.opening_hour)} - {formatHour(d.closing_hour)}
                  </Typography>
                ))}
              </Grid>
              <hr></hr>
            </CardContent>

            <CardActions className={classes.column}>
              <Typography variant="body2">
                {commerce.address}
              </Typography>
              <Button variant="outlined" color="secondary" size="small" onClick={() => history.push('/map-example')}>{t("Commerce.see_map")}</Button>
            </CardActions>
          </Card>
        </Grid>

        <Grid item xs={12}>
          <Products products={products} showCommerceButton={false} showAdminButton={false} {...props}/>
        </Grid>
      </Container>)
    }
    />)
}