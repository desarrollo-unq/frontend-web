import { useState, useEffect } from 'react';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Dialog, AppBar, Toolbar, IconButton, Typography, TextField, Grid, Select, MenuItem, Slide  } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import commerceProvider from '../api/commerce'
import productCategoryProvider from '../api/product-category'
import { registerProduct } from '../api/product'
import { getUser } from '../api/auth'
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  center:{
    display: 'flex',
    alignItems: 'center',
    alignContent: 'center',
    flexFlow: 'column',
    height: '100%'
  },
  margin: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
      width: '47%',
  },
  width: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      width: '70%',
      maxHeight: '25%',
  },
  dialog: {
      width:'80%',
      margin: 'auto',
      height: '80%',
      marginTop: '5%'
  },
  allWidth: {
      width: '100%',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

export function useModalAddProduct(){
    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
        };

    const handleClose = () => {
        setOpen(false)
        };

    return [
        () => <FullScreenDialog 
            open={open}
            handleClose={handleClose}
        />,
        handleClickOpen,

    ]
}


function FullScreenDialog({open, handleClose}) {
  const classes = useStyles()
  const {t} = useTranslation()
  const [name, setName] = useState('')
  const [trademark, setTrademark] = useState('')
  const [image, setImage] = useState('')
  const [price, setPrice] = useState(0)
  const [stock, setStock] = useState(0)
  const [selectedCategory, setSelectedCategory] = useState(0)
  const [commerce, setCommerce] = useState(null)
  const [productCategories, setProductCategories] = useState([])
  

  useEffect(() =>{
    productCategoryProvider.getAll().then(categories => setProductCategories(categories))
    commerceProvider.getCommerceByUser(getUser().id).then(commerce => setCommerce(commerce[0].id))
  }, [setProductCategories, setCommerce])

  function handleRegister() {
      registerProduct(name, trademark, image, price, stock, selectedCategory, commerce)
      handleClose(); 
      window.location.reload()
      
  }


  return (
    <div>
     
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition} className={classes.dialog}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {t('ModalAddProduct.addProduct')}
            </Typography>
            <Button autoFocus color="inherit" onClick={() =>{ handleRegister() }}>
            {t('ModalAddProduct.save')}
            </Button>
          </Toolbar>
        </AppBar>
        <Grid md="12" className={classes.center}>
            <Grid md="12" row className={classes.width}>
                <TextField 
                  className={classes.margin} 
                  id="name" 
                  label={t('ModalAddProduct.name')}
                  value={name}
                  onChange={e=>setName(e.target.value)} 
                />
                <TextField 
                  className={classes.margin} 
                  id="trademark" 
                  label={t('ModalAddProduct.trademark')} 
                  value={trademark}
                  onChange={e=>setTrademark(e.target.value)} 
                />
            </Grid>
            <Grid md="12" row className={classes.width}>
                <TextField 
                  className={classes.allWidth} 
                  id="image" 
                  label={t('ModalAddProduct.image')}
                  value={image}
                  onChange={e=>setImage(e.target.value)} 
                />
            </Grid>
            <Grid md="12" row className={classes.width}>
                <TextField 
                  className={classes.margin} 
                  id="price" 
                  label={t('ModalAddProduct.price')}
                  value={price}
                  type="number"
                  onChange={e=>setPrice(e.target.value)} 
                />
                <TextField 
                  className={classes.margin} 
                  id="stock" 
                  label="Stock" 
                  value={stock}
                  type="number"
                  onChange={e=>setStock(e.target.value)} 
                />
            </Grid>
            <Select
            id="category"
            value={selectedCategory || 0}
            onChange={e => setSelectedCategory(e.target.value)}
            >
            <MenuItem value={0} disabled>
                {t('ModalAddProduct.category')}
            </MenuItem> 
            {productCategories.map(cc => {
                return <MenuItem value={cc.id} key={cc.id}>{cc.name}</MenuItem>
            })}
            </Select>
        </Grid> 
      </Dialog>
    </div>
  );
}