import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({

  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

export function Cart() {
  const classes = useStyles();
  const history = useHistory()

  return (
    <div className={classes.root}>
      <IconButton
          aria-label="account of current user"
          aria-controls="menu-appbar"
          aria-haspopup="true"
          onClick={() => history.push('/buy')}
          color="inherit"
        >
        <ShoppingCartIcon />
      </IconButton>
    </div>
  );
}