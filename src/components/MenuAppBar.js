import React, { useState, useEffect, useMemo } from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import logo from '../resources/img/logo.png'
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { getUser, logout } from '../api/auth';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import commerceProvider from '../api/commerce';
import GTranslateIcon from '@material-ui/icons/GTranslate';
import Drawer from '@material-ui/core/Drawer';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import clsx from 'clsx';
import MenuIcon from '@material-ui/icons/Menu'
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListAltIcon from '@material-ui/icons/ListAlt'
import { Cart } from './Cart';
import { useAuth0 } from '@auth0/auth0-react';
const drawerWidth = 240;


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  image: {
    maxHeight: '40px'
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },

}));


export default function MenuAppBar(props) {
  const { t, i18n } = useTranslation();
  const {onLogout} = props
  const classes = useStyles();
  const { isAuthenticated } = useAuth0()
  const auth = useMemo(() => getUser()!==null || isAuthenticated, [isAuthenticated]);
  const [anchorEl, setAnchorEl] = useState(null);
  const history = useHistory()
  const open = Boolean(anchorEl);
  const [commerce, setCommerce] = useState(null)
  const [currentLang, setLang] = useState(i18n.language)
  const [openMenu, setOpen] = useState(false);
  const theme = useTheme();

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  
  const closeSesion = () => {
    handleClose()
    logout()
    onLogout()
  }

  function seeProfile(){
    handleClose()
    history.push('/user-profile')
  }

  function goHome(){
    history.push('/')
  }

  function handleLanguage(){
    if(currentLang === 'es'){
      i18n.changeLanguage('en')
      setLang('en')
    }
    else{
      i18n.changeLanguage('es')
      setLang('es')
    }
  }

  useEffect(() => {
    if(auth && !isAuthenticated){ commerceProvider.getCommerceByUser(getUser().id).then(commerce => setCommerce(commerce[0]))}
  }, [auth, setCommerce, isAuthenticated])
  
  return (
    <div className={classes.root}>
      <AppBar position="sticky" className={clsx(classes.appBar)}>
        <Toolbar>
          {/********** ICON RESPONSIVE MENU **********/}
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, openMenu && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
          {/********** ICON AND TITLE COMPRAMUNDO **********/}
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={goHome}>
              <img src={logo} alt="logo" className={classes.image} />
            </IconButton>
            <Typography variant="h6" className={classes.title} onClick={goHome}>
              CompraMundo
            </Typography>
          {/********** ICART **********/}
            <Cart />
          {/********** ICON USER PROFILE **********/}
          {auth && (
            <div>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
               </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={seeProfile}>Mi perfil</MenuItem>
                <MenuItem onClick={closeSesion}>Cerrar sesion</MenuItem>
              </Menu>
            </div>
          )}
        </Toolbar>
      </AppBar>

      {/********** RESPONSIVE MENU **********/}
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={openMenu}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        {auth && commerce!== undefined && <>
        <List>
            <ListItem button onClick={()=>{handleDrawerClose();history.push(`/admin-commerce`, commerce)}}>              
              <ListItemIcon> 
                <ListAltIcon/> 
              </ListItemIcon>
              <ListItemText primary={t("AdminCommerce.adminCommerce")}/>
            </ListItem>
        </List>
        </>
        }
        <Divider />
        <List>
            <ListItem button onClick={()=>{handleDrawerClose(); handleLanguage()}}>
              <ListItemIcon>
                <GTranslateIcon/>
              </ListItemIcon>
              <ListItemText primary={t("Tranlate.tranlate")}/>
            </ListItem> 
        </List>
      </Drawer>

    </div>
  );
}


export function useMenuAppBar(){
  const [user, setUser] = useState(getUser())
  const history = useHistory()
  const onLogout = () => {
    setUser(null)
    history.push('/')
  }
  return [(props) => <MenuAppBar {...props} onLogout={onLogout}/>, user]
}