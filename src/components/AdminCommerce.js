import React, { useState, useEffect } from 'react';
import CSVReader from "react-csv-reader";
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { useTabs } from './Tabs'
import { useMenuAppBar } from './MenuAppBar';
import { Fastfood, Edit, Save, StoreOutlined, AddSharp } from '@material-ui/icons'
import { Select, MenuItem, InputLabel, FormControl, Grid, TextField, Fab, Button, Divider } from '@material-ui/core'
import { useTranslation } from 'react-i18next';
import { orange, green } from '@material-ui/core/colors';
import commerceCategoryProvider from '../api/commerce-category';
import Products from './HomeContent/Products';
import commerceProvider from '../api/commerce'
import productProvider from '../api/product'
import { getUser } from '../api/auth'
import { useModalAddProduct } from './ModalAddProduct'
import { registerProduct } from '../api/product'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    flexFlow: 'column'
  },
  centercsv: {
    height: '15vh',
    alignItems: 'center',
    justifyContent: 'center',
    display: 'flex',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  image: {
    maxHeight: '40px'
  },
  fabOrange: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    backgroundColor: orange[500],
    '&:hover': {
      backgroundColor: orange[600],
    },
  },
  fabGreen: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
  color: {
    backgroundColor: green[500],
  },
  margin: {
    marginTop: "5vh",
  },
  formControl: {
    minWidth: '100%',
    marginTop: "5vh",
  },
  centerXY: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
    flexFlow: 'column',
    height: '64vh',
  },
  marginSide: {
    margin: '0px 15px'
  },
}));



export default function AdminCommerce(props) {
  const classes = useStyles()
  const history = useHistory()
  const [MenuAppBar,] = useMenuAppBar()
  const [editing, setEditing] = useState(false)
  const [commerceCategories, setCommerceCategories] = useState([])
  const [products, setProducts] = useState([])
  const commerce = props.location.state
  const [name, setName] = useState(commerce.name)
  const [address, setAddress] = useState(commerce.address)
  const [max_distance, setMaxDistance] = useState(commerce.max_distance)
  const [selectedCategory, setSelectedCategory] = useState(commerce.category.id)
  const [modal, setModal] = useState(false)
  const [ModalAddProduct, handleClickOpen] = useModalAddProduct()
  const [csvData, setCSVData] = useState([])

  const { t } = useTranslation()
  const items = [
    {
      label: t('AdminCommerce.products'),
      icon: Fastfood
    },
    {
      label: t('AdminCommerce.commerce'),
      icon: StoreOutlined
    }
  ]

  const [Tabs, currentTab] = useTabs(items)

  useEffect(() => {
    commerceCategoryProvider.getAll().then(categories => setCommerceCategories(categories))
    productProvider.getProductByCommerce(commerce.id).then(products => setProducts(products))

  }, [setCommerceCategories, setProducts, commerce.id])

  function updateCommerce() {
    commerceProvider.updateCommerce(commerce.id, name, address, max_distance, selectedCategory, getUser().id)
      .then(history.push(`/`))
      .catch(error => { console.log(error) })
  }

  function setModalAddProduct() {
    setModal(true)
    handleClickOpen()
  }

  const handleForce = (data, fileInfo) => setCSVData(data)

  const papaparseOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
    transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
  };

  function registerCSV() {
    csvData.forEach(d => {
      registerProduct(d.name, d.trademark, d.image, d.price, d.stock, d.category_id, commerce.id)
    })
  }

  function doReload() {
    setTimeout(window.location.reload(), 3000)
  }

  return (
    <div className={classes.root}>
      <MenuAppBar />
      <Tabs />

      {currentTab === 0 && <>
        {/********** Button add product **********/}
        <Fab aria-label="edit" className={editing ? classes.fabGreen : classes.fabOrange} onClick={() => setModalAddProduct()}>
          <AddSharp />
        </Fab>

        {/********** Load product from CSV **********/}
        <Grid md="12" className={classes.centercsv}>
          {t('AdminCommerce.textLoadProduct')}
          <CSVReader
            onFileLoaded={handleForce}
            parserOptions={papaparseOptions}
            cssClass={classes.marginSide}
          />
          <Button
            variant="contained"
            color="primary"
            disabled={csvData.length<=0}
            onClick={() =>{ registerCSV(); doReload()}}
          >
            {t('AdminCommerce.load')}
          </Button>
        </Grid>
        <Divider />
      </>}

      {/********** Products **********/}
      {currentTab === 0 && products.length > 0 && <>
        <Products {...props} products={products} showCommerceButton={false} showAdminButton={true} />
      </>}

      {/********** no one products **********/}
      {currentTab === 0 && products.length === 0 && <>
        <Grid md="12" className={classes.centerXY}>
          {t('AdminCommerce.noProducts')}
        </Grid>
      </>}

      {/********** Modal **********/}
      {modal && <ModalAddProduct />}

      {/********** Tab admin commerce **********/}
      {currentTab === 1 && <>
        <Grid md="12" className={classes.center}>
          <Grid md="6" >

            {/*********** Name ***********/}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              disabled={!editing}
              id="name"
              label={commerce.name}
              name="name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              autoFocus
              InputLabelProps={{ shrink: true }}
              className={classes.margin}
            />

            {/*********** Address ***********/}
            <TextField
              id="filled-full-width"
              label="Dirección"
              fullWidth
              margin="normal"
              InputLabelProps={{ shrink: true }}
              variant="outlined"
              required
              disabled={!editing}
              name="address"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
              autoFocus
              className={classes.margin}
            />

            {/*********** Distancia maxima ***********/}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              disabled={!editing}
              id="email"
              label="Radio de entrega"
              name="max_distance"
              value={max_distance}
              onChange={(e) => setMaxDistance(e.target.value)}
              autoFocus
              InputLabelProps={{ shrink: true }}
              className={classes.margin}
            />

            {/*********** Categorias ***********/}
            <FormControl className={classes.formControl}>
              <InputLabel shrink id="category-label">
                {t('CommerceDetailForm.Fields.category')}
              </InputLabel>
              <Select
                id="category"
                value={selectedCategory || 0}
                onChange={e => setSelectedCategory(e.target.value)}
                disabled={!editing}
              >
                <MenuItem value={0} disabled>
                  {t('CommerceDetailForm.Fields.select_category')}
                </MenuItem>
                {commerceCategories.map(cc => {
                  return <MenuItem value={cc.id} key={cc.id}>{cc.name}</MenuItem>
                })}
              </Select>
            </FormControl>
            {/*********** Edit commerce ***********/}
            <Fab aria-label="edit" className={editing ? classes.fabGreen : classes.fabOrange} onClick={editing ? () => {
              setEditing(!editing)
              updateCommerce()
            } : () => setEditing(!editing)}>
              {editing ? <Save /> : <Edit />}
            </Fab>

          </Grid>

        </Grid>
      </>}

    </div>
  );
}

