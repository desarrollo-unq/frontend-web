import React, { useEffect } from 'react'
import Welcome from './HomeContent/Welcome'
import { useMenuAppBar } from './MenuAppBar'
import { useAuth0 } from '@auth0/auth0-react'
import { saveUserData } from '../api/auth'

export default function Main({showWelcome, Content}) {
  const [MenuAppBar, user] = useMenuAppBar()
  const { isAuthenticated, getAccessTokenSilently } = useAuth0()

  useEffect(()=>{
    if (isAuthenticated){
      getAccessTokenSilently().then(token => {
        saveUserData(null, `Bearer ${token}`)
      })
    }
  })
  return <>
    <MenuAppBar />
    {showWelcome && <Welcome user={user} />}
    <Content />
  </>
}