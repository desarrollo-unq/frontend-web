import React, { useMemo, useState, useCallback, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Card, CardContent, Typography, Button, Select, MenuItem } from '@material-ui/core';
import Main from './Main';
import { useTranslation } from 'react-i18next';
import { create } from '../api/purchase';
import { getUser } from '../api/auth';
import { getAll } from '../api/shipping-type';
import { useSuccessSnackBar } from './Snackbar/Snackbar';
import { useHistory } from 'react-router-dom';
import { useAuth0 } from '@auth0/auth0-react';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  totalCard: {
    backgroundColor: theme.palette.secondary.paper,
    margin: theme.spacing(1, 1),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  totalCardContent: {
    padding: theme.spacing(0),
  },
  cardContent: {
    flexGrow: 1,
  },
  textRight: {
    textAlign: 'right',
    textDecoration: theme.typography.fontWeightBold
  },
  button: {
    margin: theme.spacing(1),
  },
}));

function calculateTotal(item) {
  return item.product.price * item.amount
}

function initializeCommerceProducts(items) {
  return Object.values(items.reduce((res, item) => {
    const commerce = item.product.commerce
    if (!res[commerce.id]) {
      res[commerce.id] = {
        products: [],
        subtotal: 0,
        payment_method: null,
        shipping_type: null,
        commerce: commerce
      }
    }
    res[commerce.id].products.push(item)
    res[commerce.id].subtotal += parseFloat(item.product.price) * item.amount

    return res
  }, {}))
}

function changeCommerceProductProp(original, setOriginal, update, index) {
  const newCommerceProducts = [...original]
  newCommerceProducts[index] = {
    ...newCommerceProducts[index],
    ...update
  }
  setOriginal(newCommerceProducts)
}

function Content(props) {
  const classes = useStyles();
  const items = props.location.state || []
  const history = useHistory()
  const { t } = useTranslation()
  const productsByCommerce = useMemo(() => initializeCommerceProducts(items), [items])
  const [commerceProducts, setCommerceProducts] = useState(productsByCommerce)
  const [showError, setShowError] = useState(false)
  const [shippingTypes, setShippingTypes] = useState([])
  const [Snackbar, openSnackbar] = useSuccessSnackBar(`Se confirmo tu compra!`, 'bottom', 'center')
  const { isAuthenticated, getAccessTokenSilently } = useAuth0()

  useEffect(() => {
    getAll().then(shipping_types => {
      setShippingTypes(shipping_types)
    })
  }, [setShippingTypes])


  const changeCommerceProduct = useCallback((update, index) => changeCommerceProductProp(commerceProducts, setCommerceProducts, update, index), [commerceProducts])

  function paymentMethodChange(payment_method, index) {
    changeCommerceProduct({ payment_method }, index)
  }

  function shippingTypeChange(shipping_type, index) {
    changeCommerceProduct({ shipping_type }, index)
  }

  function validateAndCreate(buy) {
    if (getUser() !== null || isAuthenticated) {
      setShowError(false)
      const errorIndex = buy.purchase_commerces.findIndex(pc => pc.payment_method === null || pc.shipping_type === null)
      if (errorIndex !== -1) {
        const newCommerceProducts = [...commerceProducts]
        newCommerceProducts[errorIndex].fields_error = true

        setCommerceProducts(newCommerceProducts)
      }
      else {
        buy.purchase_commerces = buy.purchase_commerces.map(pc => {
          return {
            commerce_id: pc.commerce.id,
            payment_method_id: pc.payment_method,
            total: pc.subtotal,
            shipping_type_id: pc.shipping_type,
            products: pc.products.map(prod => {
              return {
                product_id: prod.product.id,
                amount: prod.amount
              }
            })
          }
        })
        if (isAuthenticated)
          getAccessTokenSilently().then(token => {
            localStorage.setItem('token', token)
            create(buy).then(() => {
              openSnackbar()
              setTimeout(() => {
                history.push('/')
              }, 1000);
            })
          }).catch(e => console.log(e))
        else {
          create(buy).then(() => {
            openSnackbar()
            setTimeout(() => {
              history.push('/')
            }, 1000);
          })
        }
      }
    }
    else {
      setShowError(true)
    }
  }

  return <>
    <Snackbar />
    <Grid container className={classes.root}>
      <Grid item xs={12}>
        <Card className={classes.totalCard}>
          <CardContent>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <Typography gutterBottom variant="h5" align='center'>
                Total ${items.reduce((prev, item) => {
                return prev + calculateTotal(item)
              }, 0)}
              </Typography>
              <Button
                variant="contained"
                color="primary"
                size="small"
                fullWidth
                onClick={() => { validateAndCreate({ purchase_commerces: commerceProducts }) }}
              >
                {t('BuyDetail.confirm')}
              </Button>
            </div>
            {showError && <Typography variant='caption' color='error' align='center'>{t('BuyDetail.error')}</Typography>}
          </CardContent>
        </Card>
      </Grid>
      {commerceProducts.map((item, index) => {
        return <Grid item xs={12}>
          <Card className={classes.card}>
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant="h5" component="h2">
                {item.commerce.name}
              </Typography>
              <div >
                <Grid container>
                  <Grid item xs={4} style={{ display: 'flex', flexDirection: 'column' }}>
                    <Select
                      id="category"
                      value={item.payment_method || 0}
                      onChange={e => paymentMethodChange(e.target.value, index)}
                    >
                      <MenuItem value={0} disabled>
                        {t('BuyDetail.payment')}
                      </MenuItem>
                      {item.commerce.payment_methods.map(relation => {
                        const cc = relation.payment_method
                        return <MenuItem value={cc.id} key={cc.id}>{cc.name}</MenuItem>
                      })}
                    </Select>
                    <Select
                      id="category"
                      value={item.shipping_type || 0}
                      onChange={e => shippingTypeChange(e.target.value, index)}
                    >
                      <MenuItem value={0} disabled>
                        {t('BuyDetail.shipping_type')}
                      </MenuItem>
                      {shippingTypes.map(cc => {
                        return <MenuItem value={cc.id} key={cc.id}>{cc.name}</MenuItem>
                      })}
                    </Select>
                    {item.fields_error && <Typography variant='caption' color='error'>{t('BuyDetail.fields_error')}</Typography>}
                  </Grid>
                </Grid>
              </div>
              <Typography variant="h6" className={classes.textRight}>
                Subtotal ${item.subtotal}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      })}
    </Grid>
  </>
}
export default function BuyDetail(props) {
  return <Main showWelcome={false} Content={() => <Content {...props} />} />
}
