import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { TextField, RadioGroup, Radio, FormControlLabel } from '@material-ui/core'
import SortIcon from '@material-ui/icons/Sort';
import Product from './Product'
import { useTranslation } from 'react-i18next'
import { orange, green } from '@material-ui/core/colors'

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  cardGrid: {
    paddingBottom: theme.spacing(8),
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  textCenter: {
    textAlign: 'center'
  },
  formControl: {
    margin: theme.spacing(4),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  button: {
    margin: theme.spacing(1),
  },
  elementCenter:{
    display: 'flex',
    justifyContent: 'center !important',
    alignItems: 'center',
  },
  fabOrange: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    backgroundColor: orange[500],
    '&:hover': {
      backgroundColor: orange[600],
    },
  },
  fabGreen: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[600],
    },
  },
}));


export function useProducts(listProducts) {
  const [products, setProducts] = useState(listProducts)

  function addProduct(product) {
    setProducts(prev => {
      prev.push(product)   
      return prev
    })
  }
  return [props => <Products {...props} products={products} />, addProduct]
}

export default function Products({addProduct, products, showCommerceButton, showAdminButton}) {
  const classes = useStyles();
  const [foldProducts, setFoldProducts] = useState(products)
  const [filtered, setFiltered] = useState(false)
  const [showFilter, setShowFilter] = useState(false)
  const [selectedPriceFilter, setSelectedPriceFilter] = useState('price-none');
  const { t } = useTranslation();

  const sortProducts = event => {
    const type = event.target.value

    if (type === 'price-none') {
      setFoldProducts(products)
      setFiltered(false)
    }
    else {
      const sorted = products.sort((a, b) => type === 'price-higher' ? b.price - a.price : a.price - b.price);
      setFoldProducts(sorted);
      setFiltered(true)
    }
    setSelectedPriceFilter(type)
    setShowFilter(false)
  };

  const search = e => {
    const filtered = products.filter(p => p.name.toUpperCase().includes(e))
    setFoldProducts(filtered)
  }

  

  return (
    <>
      <CssBaseline />
      <main>
        <Container className={classes.cardGrid} maxwidth="md">
          <TextField
            id="standard-search"
            label={t("Products.Search")}
            type="search"
            fullWidth
            onChange={(e) => search(e.target.value.toUpperCase())}
          />
          <Button
            variant="outlined"
            color="default"
            className={classes.button}
            startIcon={<SortIcon color={filtered ? "primary" : "default"} />}
            onClick={() => {
              setShowFilter(!showFilter)
            }}
          >
            {t("Products.Order")}
          </Button>
          {showFilter &&
            <RadioGroup aria-label="gender" name="gender1" value={selectedPriceFilter} onChange={sortProducts}>
              <FormControlLabel value="price-none" control={<Radio />} label={t("Products.None")} />
              <FormControlLabel value="price-higher" control={<Radio />} label={t("Products.HigherPrice")} />
              <FormControlLabel value="price-lower" control={<Radio />} label={t("Products.LowerPrice")} />
            </RadioGroup>}
          <Grid container spacing={4}>
            {foldProducts.map(product => (
              <Product key={product.id} product={product} onAddProduct={addProduct} showCommerceButton={showCommerceButton} showAdminButton={showAdminButton}/>
            ))}
          </Grid>
        </Container>
      </main>
    </>
  );
}