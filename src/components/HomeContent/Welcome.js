import React from 'react';
import { Container, Typography, Grid, Button, makeStyles, useMediaQuery, useTheme } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useAuth0 } from '@auth0/auth0-react';

const useStyles = makeStyles((theme) => ({
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 4),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
}));

export default function Welcome({ user }) {
  const classes = useStyles();
  const theme = useTheme()
  const sm = useMediaQuery(theme.breakpoints.up('sm'));
  const history = useHistory()
  const { t } = useTranslation();
  const { isAuthenticated } = useAuth0()

  return <div className={classes.heroContent}>
    <Container maxWidth="sm">
      <Typography component="h1" variant={sm ? "h2" : "h4"} align="center" color="textPrimary" gutterBottom>
        {t('Welcome.Title')}!
      </Typography>
      <Typography variant={sm ? "h5" : 'h6'} align="center" color="textSecondary" paragraph>
        {t('Welcome.Slogan')}
      </Typography>
      {!user && !isAuthenticated && <div className={classes.heroButtons}>
        <Grid container spacing={2} justify="center">
          <Grid item>
            <Button variant="contained" color="primary" onClick={() => history.push('/login')}>
              {t('Welcome.Login')}
            </Button>
          </Grid>
          <Grid item>
            <Button variant="outlined" color="primary" onClick={() => history.push('/registration')}>
              {t('Welcome.Register')}
            </Button>
          </Grid>
          <Grid item>
            <Button variant="outlined" color="secondary" onClick={() => history.push('/map-example')}>
              {t('Welcome.Map')}
            </Button>
          </Grid>
        </Grid>
      </div>}
    </Container>
  </div>
}
