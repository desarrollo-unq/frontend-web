import React, { useState, useContext, useCallback } from 'react';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';
import { useSuccessSnackBar } from '../Snackbar/Snackbar';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import EditIcon from '@material-ui/icons/Edit'
import DeleteIcon from '@material-ui/icons/Delete'
import { useModalEditionProduct } from '../ModalEditionProduct'
import { store } from '../../CartContext';

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  textCenter: {
    textAlign: 'center',
  },
  button: {
    margin: theme.spacing(1),
  },
  pointer: {
    cursor: 'pointer'
  }
}));


export default function Product({ product, onAddProduct, showCommerceButton, showAdminButton }) {
  const classes = useStyles()
  const history = useHistory()
  const theme = useTheme()
  const [showAmount, setShowAmount] = useState(false)
  const [snackbar, setShowSnackbar] = useState(false)
  const [modal, setModal] = useState(false)
  const [ModalEditionProduct, openModal, setTypeModalReducer, typeModal] = useModalEditionProduct(product)
  const [amount, setAmount] = useState("")
  const [Snackbar, openSnackbar] = useSuccessSnackBar(`Se agregaron ${amount} ${product.name} al carrito`, 'bottom', 'center')
  const { dispatch } = useContext(store)
  const { t } = useTranslation();

  function showSnackbar() {
    dispatch({type:'add-product', item: { product, amount }})
    setShowSnackbar(true)
    openSnackbar()
    setShowAmount(false)
  }
  
  function setModalReducer(typeModal){
    setModal(true)
    openModal()
    setTypeModalReducer(typeModal)
    
  }

  return (
    <Grid item key={product} xs={12} sm={6} md={4}>
      <Card className={classes.card}>
        <CardMedia
          className={classes.cardMedia}
          image={product.image}
          title={`Imagen de ${product.name}`}
        />
        <CardContent className={classes.cardContent}>
          <Typography gutterBottom variant="h5" component="h2">
            {product.name}
          </Typography>
          <Typography gutterBottom variant={"subtitle1"}>
            {product.trademark}
          </Typography>
          <Typography className={classes.textCenter}>
            ${product.price}
          </Typography>
        </CardContent>
        <CardActions>
          {snackbar && <Snackbar />}
          {showCommerceButton && <Button
            variant="outlined"
            color="primary"
            size="small"
            onClick={() => history.push(`/commerce/${product.commerce.id}`, product.commerce)}
          >
            {t("Product.SeeCommerce")}
          </Button>}
          {!showAmount && !showAdminButton && <Button
            size="small"
            color="primary"
            onClick={() => setShowAmount((prev) => !prev)}
          >
            {t("Product.AddToCart")}
          </Button>}
          {showAdminButton && <>
              <EditIcon color="primary" className={classes.pointer} onClick={() => setModalReducer(false)}></EditIcon>
              <DeleteIcon color="primary" className={classes.pointer} onClick={ () => setModalReducer(true)}></DeleteIcon>
            </>
          }
        </CardActions>

        {showAmount && <>
          <TextField
            value={amount}
            variant="outlined"
            placeholder="Cantidad"
            label="Cantidad"
            required
            style={{ margin: theme.spacing(2) }}
            onChange={e => setAmount(e.target.value)}>
          </TextField>
          <Button variant='contained' style={{ margin: theme.spacing(2) }} color="primary" onClick={showSnackbar}>
            Agregar
          </Button>
        </>
        }
      </Card>
        {/********** MODAL EDIT/DELETE PRODUCT **********/}
        {modal && <ModalEditionProduct typeModal={typeModal} product={product}/>}
        
    </Grid>
  );
}