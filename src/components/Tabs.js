import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';


const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

export function CenteredTabs({value, handleChange, items}) {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="primary"
        textColor="primary"
        centered
      >
        {
          items.map(item => {
            return <Tab label={item.label} icon={item.icon && <item.icon />} />
          })
        }
      </Tabs>
    </Paper>
  );
}

export function useTabs(items){
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return [(props) => <CenteredTabs {...props} items={items} value={value} handleChange={handleChange} />, value]
}