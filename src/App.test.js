import { API_URL } from './api/base';

test('api url is correct', () => {
  expect(API_URL).toEqual('https://compramundo-admin.herokuapp.com/api/')
})