import {API_URL, public_api, private_api} from './base'

const AUTH_URL = `${API_URL}auth/`
export function login(email, password) {
  return new Promise((resolve) => {
    public_api.post(`${AUTH_URL}login/`, {email, password}).then(response=>{
      _saveUserData(response.data)
      resolve(response.data)
    })
  })
}

export function register(email, password){
  return new Promise((resolve) => {
    public_api.post(`${AUTH_URL}register/`, {email, password}).then(response=>{
      _saveUserData(response.data)
      resolve(response.data)
    })
  })
}

export function logout(){
  sessionStorage.removeItem('user')
  sessionStorage.removeItem('token')
}

export function saveUser(email, first_name, last_name){
  return new Promise((resolve) => {
    public_api.put(`${AUTH_URL}${getUser().id}/`, {email, first_name, last_name}).then(response=>{
      _saveUserData(response.data)
      resolve(response.data)
    })
  })
}

export function updatePassword(old_password, new_password){
  return new Promise((resolve) => {
    private_api.post(`${AUTH_URL}change_password/`, {old_password, new_password}).then(response=>{
      _saveUserData(response.data)
      resolve(response.data)
    })
  })
}

export function getUser(){
  return JSON.parse(sessionStorage.getItem('user'))
}

function _saveUserData(data){
  saveUserData(data.user, `Token ${data.token}`)
}

export function saveUserData(user, token){
  sessionStorage.setItem('user', JSON.stringify(user))
  sessionStorage.setItem('token', JSON.stringify(token))
}