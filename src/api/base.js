import axios from 'axios'

export const public_api = axios.create()
export const private_api = axios.create()

private_api.interceptors.request.use(function (config) {
  config.headers.Authorization = getToken()
  return config;
});

export function getToken(){
  return JSON.parse(sessionStorage.getItem('token'))
}

export const API_URL = "https://compramundo-admin.herokuapp.com/api/"
//export const API_URL = "http://localhost:8000/api/"
