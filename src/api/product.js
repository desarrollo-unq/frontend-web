import {API_URL, public_api} from './base'

const PRODUCT_URL = `${API_URL}product/`

export function getAll(){
  return new Promise((resolve, reject) => {
    public_api.get(PRODUCT_URL).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function getProductByCommerce(idCommerce){
  return new Promise((resolve, reject) => {
    public_api.get(`${PRODUCT_URL}?commerce=${idCommerce}`).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function deleteProduct(idProduct){
  return new Promise((resolve, reject) => {
    public_api.delete(`${PRODUCT_URL}${idProduct}`).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function registerProduct(name, trademark, image, price, stock, category, commerce) {
  const payload = {
    name,
    trademark,
    image,
    price,
    stock,
    category_id: category,
    commerce_id: commerce
  }
  return new Promise((resolve, reject) => {
    public_api.post(PRODUCT_URL, payload).then(response => {
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function updateProduct(id, name, trademark, price, stock, image, commerce_id, category_id){
  return new Promise((resolve) => {
    public_api.put(`${PRODUCT_URL}${id}/`, {name, trademark, price, stock, image, commerce_id, category_id}).then(response=>{
      resolve(response.data)
    }).catch(error => {
      console.log(error)
    })
  })
}

export default {getAll, getProductByCommerce, deleteProduct, updateProduct}