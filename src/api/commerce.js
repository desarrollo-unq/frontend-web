import { API_URL, public_api } from './base'

const COMMERCE_URL = `${API_URL}commerce/`
const COMMERCE_PAYMENT_METHOD_URL = `${API_URL}commerce-payment-method/`

export function registerCommerce(supervisor, name, address, max_distance, category) {
  const payload = {
    supervisor,
    name,
    address,
    max_distance,
    category_id: category
  }
  return new Promise((resolve, reject) => {
    public_api.post(COMMERCE_URL, payload).then(response => {
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function addPaymentMethods(commerce, paymentMethods) {
  const payload = paymentMethods.map(payment_method_id => {
    return {
      commerce: commerce.id,
      payment_method_id
    }
  })

  return new Promise((resolve, reject) => {
    public_api.post(`${COMMERCE_PAYMENT_METHOD_URL}create_many/`, payload).then(response => {
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function getPaymentMethodByCommerce(idCommerce){
  return new Promise((resolve, reject) => {
    public_api.get(`${COMMERCE_PAYMENT_METHOD_URL}?commerce=${idCommerce}`).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function getCommerce(id){
  return new Promise((resolve, reject) => {
    public_api.get(`${COMMERCE_URL}${id}`).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function getCommerceByUser(idUser){
  return new Promise((resolve, reject) => {
    public_api.get(`${COMMERCE_URL}?user=${idUser}`).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function updateCommerce(id, name, address, max_distance, category_id, supervisor){
  return new Promise((resolve) => {
    public_api.put(`${COMMERCE_URL}${id}/`, {name, address, max_distance, category_id, supervisor}).then(response=>{
      resolve(response.data)
    }).catch(error => {
      console.log(error)
    })
  })
}

export default {getCommerce, getCommerceByUser, getPaymentMethodByCommerce, updateCommerce}