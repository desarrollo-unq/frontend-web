import {API_URL, public_api} from './base'

const PRODUCT_CATEGORY_URL = `${API_URL}product-category/`

export function getAll(){
  return new Promise((resolve, reject) => {
    public_api.get(PRODUCT_CATEGORY_URL).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export default {getAll}