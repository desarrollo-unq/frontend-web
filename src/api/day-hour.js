import {API_URL, public_api} from './base'

const DAY_HOUR_URL = `${API_URL}day-hour/`

export function getAllByCommerce(idCommerce){
  return new Promise((resolve, reject) => {
    public_api.get(`${DAY_HOUR_URL}?commerce=${idCommerce}`).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function addDayHours(commerce, dayHours){
  const payload = dayHours.map(dayhour => {
    return {
      commerce: commerce.id,
      day: dayhour.day,
      opening_hour: dayhour.openingHour.format('HH:mm'),
      closing_hour: dayhour.closingHour.format('HH:mm'),
    }
  })
  return new Promise((resolve, reject) => {
    public_api.post(`${DAY_HOUR_URL}create_many/`, payload).then(response=>{
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export default {getAllByCommerce}