import { API_URL, private_api } from './base'

const PURCHASE_URL = `${API_URL}purchase/`

export function getByUser(user) {
  return new Promise((resolve, reject) => {
    private_api.get(`${PURCHASE_URL}get_by_user`).then(response => {
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}

export function create(buy) {
  return new Promise((resolve, reject) => {
    private_api.post(PURCHASE_URL, buy).then(response => {
      resolve(response.data)
    }).catch(error => reject(error))
  })
}