import { API_URL, public_api } from './base'

const SHIPPING_TYPE = `${API_URL}shipping-type/`

export function getAll() {
  return new Promise((resolve, reject) => {
    public_api.get(SHIPPING_TYPE).then(response => {
      resolve(response.data)
    }).catch(error => {
      reject(error)
    })
  })
}